var fs = require('fs');
const express = require('express')
const { Samsung, KEYS, APPS } = require('samsung-tv-control')
const jsonfile = require('jsonfile')
// Location of File Cache
const file = '/home/token.json'
var https = require('https');

// Set up SSL
var privateKey  = fs.readFileSync('/cert/cert.key', 'utf8');
var certificate = fs.readFileSync('/cert/file.cert', 'utf8');
var credentials = {key: privateKey, cert: certificate};

// Keep BASE URL in case its needed
let baseURL = "https://tv.grwh.work"

// Enable CORS for Express
var cors = require('cors')
// Use These options to allow ALL
const corsOptions ={
  origin:'*', 
  credentials:true,      
  optionSuccessStatus:200,
}

// Use JSON NPM to read the token from cache
jsonfile.readFile(file, function (err, obj) {
  if (err) console.error(err)
  // The token was read, here it is
  console.log("token: " + obj.data)
  // Lets add that to the configuration
  const config = {
    debug: false, // Default: false
    ip: '192.168.0.2',
    mac: 'A4:30:7A:08:57:6A',
    nameApp: 'WebControl', // Default: NodeJS
    port: 8002, // Default: 8002
    token: obj.data,
  }

  // Now we have a config, lets set it
  const control = new Samsung(config)
    // Get all installed apps from TV - FUNCTION
    control.getAppsFromTV((err, res) => {
      console.log(err)
      if (!err) {
        console.log('# Response getAppsFromTV', res)
      }
    })

  // Function to send KEY!
  async function sendKey(key) {
    // Send key to TV
    control.sendKey(key, function (err, res) {
      if (!err) {
        console.log("Sending: " + key)
      }
    })
  }
  // Function to send INPUT!
  async function sendInput(input){
       // Send text to focused input on TV
       control.sendText(input, function (err, res) {
        if (!err) {
          console.log("Sending: " + input)
          console.log('# Response sendText', res)
        }
      })
  }
  // Defube Express and https serer
  const app = express()
  var httpsServer = https.createServer(credentials, app);
  // Set port
  const port = 3031
  // USe CORS
  app.use(cors(corsOptions))

  // Lets handle BLANK Traffic!
  app.get('/', (req, res) => {
    res.end('Welcome to the TV API!')

  })
  
  // Handling home
  app.get('/home', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_HOME)
      res.end('1')
    })();
  })

    // Handling right
  app.get('/right', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_RIGHT)
      res.end('1')
    })();
  })

    // Handling left
  app.get('/left', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_LEFT)
      res.end('1')
    })();
  })

    // up
  app.get('/up', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_UP)
      res.end('1')
    })();
  })

    // Handling down
  app.get('/down', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_DOWN)
      res.end('1')
    })();
  })

    // Handling enter
  app.get('/enter', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_ENTER)
      res.end('1')
    })();
  })

    // Handling back
  app.get('/back', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_RETURN)
      res.end('1')
    })();
  })

    // Handling volume down
  app.get('/volumeDown', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_VOLDOWN)
      res.end('1')
    })();
  })

    // Handling volume up
  app.get('/volumeUp', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_VOLUP)
      res.end('1')
    })();
  })

  // Handling mute
  app.get('/mute', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_MUTE)
      res.end('1')
    })();
  })

    // Handling picture mode
  app.get('/pmode', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_PMODE)
      res.end('1')
    })();
  })

  // Handling off
  app.get('/off', (req, res) => {
    (async () => {
      await sendKey(KEYS.KEY_POWEROFF)
      res.end('1')
    })();
  })


  // Handling input
  app.get('/input', (req, res) => {
    const path = require('path');
    (async () => {
      let sent = req.query.data
      await sendInput(sent)
      res.sendFile(path.join(__dirname, '/finish_input.html'));
    })();
  })


  // Generating a new token
  app.get('/newToken', (req, res) => {
    (async () => {
      control
        .isAvailable()
        .then(() => {
          // Get token for API
          control.getToken((token) => {
            console.info('# Response getToken:', token)
            const obj = { data: token }
            if (token == null) return
            jsonfile.writeFile(file, obj, function (err) {
              if (err) console.error(err)
              console.log("new key, exiting to refresh")
              process.exit(0);
            })
          })
        })
      res.end('1')
    })();
  })

  // Hadling checking password
  app.get('/checkPass', (req, res) => {
    (async () => {
      let pass = "changeme"
      let sent = req.query.pass
      if (pass == sent){
        console.log("User Authed Properly!")
        res.end('1')
      } else {
        res.end('0')
      }
    })();
  })

  // Listen
  httpsServer.listen(port, () => {
    console.log(`TV Control API listening on port ${port}`)
  })
})
